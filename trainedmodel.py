from sklearn.externals import joblib
from sklearn.preprocessing import MinMaxScaler
from keras.models import load_model
import keras.backend as K
import numpy as np

class TrainedModel:
    """ Using framework from S.Hirlander June 2019
    to plug into AI Gym environment
    """

    def __init__(self, load_path='crystal_001/'):
        self.scaler_in = joblib.load(load_path + 'scaler_in.save')
        self.scaler_out = joblib.load(load_path + 'scaler_out.save')
        #self.keras_model = load_model(load_path + 'Crystal_SM_model_3d_01.h5')
        self.keras_model = load_model(load_path + 'Crystal_SM_model_3d_02.h5')
        self.sess = K.get_session()

    def predict(self, x):
        """ Does scaling and prediction.
        (x, y): physics space
        (x_tilde, y_tilde): scaled space (as used for NN training)
        """
        if len(x.shape) == 1:
            x = x.reshape(1, -1)
        x_tilde = self.scaler_in.transform(x)
        y_tilde = self.keras_model.predict(x_tilde)
        y = self.scaler_out.inverse_transform(y_tilde)
        return y

    def get_jacobian(self, x):
        """ Inspired by:
        https://medium.com/unit8-machine-learning-publication/ \
        computing-the-jacobian-matrix-of-a-neural-network-in- \
        python-4f162e5db180 
        Using chain rule to account for scalers (in and out) """
        jacobian = []
        n_outputs = self.keras_model.output_shape[1]
        if len(x.shape) == 1:
            x = x.reshape(1, -1)
        x_tilde = self.scaler_in.transform(x)[0]

        for m in range(n_outputs):
            grad_func = K.gradients(self.keras_model.output[:, m],
                                    self.keras_model.input)
            grads = self.sess.run(
                grad_func,
                feed_dict={self.keras_model.input:
                           x_tilde.reshape((1, x_tilde.size))})
            jacobian.append(grads[0][0, :])

        # Apply chain rule using derivatives of scaler_in and
        # scaler_out^{-1}
        deriv_scaler_in = np.diag(self.scaler_in.scale_)
        deriv_scaler_out_inv = np.diag(1./self.scaler_out.scale_)
        jacobian = np.dot(np.array(jacobian), deriv_scaler_in)
        jacobian = np.dot(deriv_scaler_out_inv, jacobian)

        return jacobian

    def get_jacobian_approx(self, x, eps=1e-2):
        jacobian = []
        if len(x.shape) == 1:
            x = x.reshape(1, -1)
        f1 = self.predict(x)
        n_inputs = x.shape[1]
        for i in range(n_inputs):
            x2 = x.copy()
            x2[:, i] += eps
            f2 = self.predict(x2)
            jacobian.append((f2 - f1)[0] / eps)
        return np.array(jacobian).T

# Reload trained model
# To make predictions, use y = model.predict(x):
# x has shape (n_samples, 3), i.e. 3 features: crystal_position, crystal_angle, ZS_angle
# y has shape (n_samples, 1), total_loss

case_dir = 'crystal_001/'
trained_model = TrainedModel(case_dir)